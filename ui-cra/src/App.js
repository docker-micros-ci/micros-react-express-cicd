import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

const { REACT_APP_API_URL } = process.env;

function App() {
  const [message, setMessage] = useState();
  useEffect(() => {
    axios.get(REACT_APP_API_URL)
      .then(response => setMessage(response.data.message));
  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Message from Star Command:
        </p>
        <p style={{ color: 'darksalmon' }}>
          {message || 'Loading...'}
        </p>
        <p style={{ fontSize: '0.5em' }}>
          Change this message via the API <code>app.js</code> file and re-commit.
        </p>
      </header>
    </div >
  );
}

export default App;
